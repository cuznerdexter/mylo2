/**
* mylo.servicesAuth Module
*
* Description
*/
angular.module('mylo.servicesAuth', [])

.factory('ServicesAuth', ['$firebaseAuth', '$rootScope', '$state', 
    function ($firebaseAuth, $rootScope, $state) {
  
    var servicesAuthInst = {};
    servicesAuthInst.baseUrl = 'https://mylo.firebaseio.com';
    servicesAuthInst.authRef = new Firebase(servicesAuthInst.baseUrl);
    servicesAuthInst.Auth = $firebaseAuth(servicesAuthInst.authRef);
    servicesAuthInst.CurrName = '';
    servicesAuthInst.CurrUserId = '';
    servicesAuthInst.CurrUserEmail = 'BLAH';
    servicesAuthInst.userPath = '';
    servicesAuthInst.statsPath = '';
    servicesAuthInst.statsEntryPath = '';
    servicesAuthInst.statsCurrDate = '';
    servicesAuthInst.loggedIn = false; //default
    servicesAuthInst.simplelogin = '';

    servicesAuthInst.loginGrp = [
        {   'name': 'Facebook',
            'img': 'img/logos/FB-f-Logo__blue_50.png',
            'auth': 'parentObj.auth.$authWithOAuthPopup("facebook", {scope: "email"})'            
        },
        {   'name': 'Twitter',
            'img': 'img/logos/Twitter_logo_blue_50.png',
            'auth': 'parentObj.auth.$authWithOAuthPopup("twitter")'
        },
        {   'name': 'Google+',
            'img': 'img/logos/google_logo_red_50.png',
            'auth': 'parentObj.auth.$authWithOAuthPopup("google", {scope: "email"})'
        },
        {   'name': 'Guest user',
            'img': 'img/logos/mylo-logo-192.png',
            'auth': 'parentObj.auth.$authAnonymously()'
        },
        {   'name': 'MYLO email',
            'img': 'img/logos/mylo-logo-192.png',
            'auth': 'parentObj.auth.$authWithPassword()'
        }        
    ]; 

    servicesAuthInst.lastLogin = function () {
        var date = new Date();
        var lastlog = Date.now();
        return lastlog;
    };

    servicesAuthInst.Login = function (authData) {
       
        var str = authData.uid;        
        var strInd = str.indexOf('simple');
        var strName = '';
        if(strInd != -1)
        {            
            strName = str;      
        }        
        switch (authData.provider){
            case 'facebook':
                servicesAuthInst.CurrUserEmail = authData.facebook.email;
                break;
            case 'twitter':
                servicesAuthInst.CurrUserEmail = authData.twitter.username;
                break;
            case 'google':
                servicesAuthInst.CurrUserEmail = authData.google.email;                
                break;  
            case 'password':
                servicesAuthInst.CurrUserEmail = authData.password.email;
                break;
            default:
                   
        }
        if(servicesAuthInst.CurrUserEmail === undefined)
        {
            servicesAuthInst.CurrUserEmail = 'emailNotGiven'; 
        }
        servicesAuthInst.CurrUserId = authData.uid;
        //set user firebase path
        servicesAuthInst.userPath = new Firebase(servicesAuthInst.baseUrl + '/users/' + servicesAuthInst.CurrUserId  );        
        servicesAuthInst.userPath.update({
            'id': authData.uid,
            'email': servicesAuthInst.CurrUserEmail,
            'loglast':  servicesAuthInst.lastLogin(),
            'loggedIn': true
        });
        servicesAuthInst.loggedIn = true;
    };

    servicesAuthInst.Logout = function() {       
        servicesAuthInst.userPath.update({            
            'loggedIn': false
        });
        servicesAuthInst.authRef.unauth();
        servicesAuthInst.loggedIn = false;
        $state.go('app.home');
    };

    servicesAuthInst.getSimpleLogin = function () {       
        return servicesAuthInst.simplelogin;
    };
    servicesAuthInst.setSimpleLogin = function (simpleloginTxt) {
        var strRpl = simpleloginTxt.replace('.', ',');
        servicesAuthInst.simplelogin = strRpl;       
    };

    servicesAuthInst.Anonamous = function(error, authData) {
        servicesAuthInst.Auth.$authAnonymously().then(function (error, authData) {
            if(error) {
                console.log("Login Failed!", error);
            } else {
                console.log("Authenticated successfully with payload:", authData);
            }
        });  
           
    };

    servicesAuthInst.UpdateData = function (authData) {        
        switch (authData.provider){
            case 'facebook':
                servicesAuthInst.CurrName = authData.facebook.displayName;
                break;
            case 'twitter':
                servicesAuthInst.CurrName = authData.twitter.displayName;
                break;
            case 'google':
                servicesAuthInst.CurrName = authData.google.displayName;                
                break;            
        }
    };


    //check for auth change
    servicesAuthInst.CheckAuthState = function (authData) {           
        if(authData)
        {           
            servicesAuthInst.Login(authData);
            servicesAuthInst.UpdateData(authData);
            $state.go('app.home');
        }else {
          
        }
    };

    servicesAuthInst.getLoggedInStatus = function () {
        return servicesAuthInst.loggedIn;
    };

    servicesAuthInst.authRef.onAuth(servicesAuthInst.CheckAuthState);

    return servicesAuthInst;
}]);