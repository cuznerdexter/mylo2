angular.module('mylo.directives', [])


.directive('swapcolour', ['$rootScope', '$state', '$controller', 'medLengthService',  
    function ($rootScope, $state, $controller, medLengthService) {
  
    var pageArr = [{home:'redBg'}, {meditated:'greenBg'}, {meditationLength:'yellBg'}, {meditationThoughtless:'blueBg'}, {meditationDay: 'blueBg'}, {meditationStats:'purpBg'}, {login:'blueBg'}, {meditationReflect:'blueBg'}, {meditationAdvice:'redBg'}];
    
    return function (scope, element, attrs) {  
        var nextPageNum = parseInt(attrs.mylonxtpage); 
        
        if($state.current.name == 'app.login')
        {
            nextPageNum = $rootScope.mylosaved.state;
        }

        try {
             
            if (isNaN(nextPageNum)) { throw element.text() + ' mylonxtpage is NOT a Number'; }      
            
            var obj = pageArr[nextPageNum];
            var key = Object.keys(obj);
            var keyVal = obj[key];           

            element.bind('click', function () {
                if($state.current.name == "meditationLength")
                {
                    if(medLengthService.timeSliderVal == 'undefined') {medLengthService.timeSliderVal = 0;}
                    
                    if(medLengthService.timeSliderVal < 40)
                    {
                        $state.transitionTo('app.meditationReflect');                 
                        $rootScope.$$childHead.parentObj.colorVal1 = 'blueBg';                        
                    
                    }else {
                        $state.transitionTo('app.' + key[0]);                 
                        $rootScope.$$childHead.parentObj.colorVal1 = keyVal;                    
                    }         
                }else {
                    $state.transitionTo('app.' + key[0]);                 
                    $rootScope.$$childHead.parentObj.colorVal1 = keyVal;                   
                }                
            });        

        } catch(e) {         
        }        
    };
}])



.directive('sidemenubut', ['$document', '$ionicGesture', 
    function ($document, $ionicGesture) {     
        
        return {
            scope: {
              control: '='
            },
            
            link: function (scope, element, attr) {
 
                var documentResult = document.getElementsByTagName("body");               
                var wrappedDocumentResult = angular.element(documentResult);                

                $ionicGesture.on('touch', function(e){                    
                    if(wrappedDocumentResult.hasClass('menu-open'))
                    {                  
                        element.removeClass('opened'); 
                    }else {                    
                        element.addClass('opened'); 
                    }
                }, element);

            }
        };
}])






.directive('dragvert', ['$document', '$ionicGesture', 'medLengthService', 
    function($document, $ionicGesture, medLengthService) {   

    return function (scope, element, attr) {     
        var currY = attr.currypos;        
        var startX = 0, startY = 0, x = 0, y = 0, markerYOffset, markerXOffset, parentDiv, parentTop, parentBot, parent1perc, parent2perc, parent12perc, parent25perc, parent37perc, parent50perc, parent62perc, parent75perc, parent90perc, parentPercArr, timeLineArr, timelineCurr;

        markerYOffset = element[0].offsetHeight / 2;
        markerXOffset = element[0].offsetWidth / 2;
        parentDiv = element.parent();
        parentTop = 0;
        parentBot = (parentDiv[0].offsetHeight) - markerYOffset*1.5 ;
        parent1perc = parentBot /90;
        parent2perc = parent1perc * 2;
        parent5perc = parent1perc * 5;
        parent12perc = parent1perc * 12.5;
        parent25perc = parent1perc * 25;
        parent37perc = parent1perc * 37.5;
        parent50perc = parent1perc * 50;
        parent62perc = parent1perc * 62.5;
        parent75perc = parent1perc * 75;
        parent90perc = parent1perc * 90;
        parentPercArr = [
            parent1perc,parent12perc,parent25perc,parent37perc,parent50perc,parent62perc,parent75perc,parent90perc
        ];
        percentValArr = [
            0,          5,          10,          15,           20,         25,          30,          35
        ];
        timeLineArr = [];

        angular.forEach(document.querySelectorAll('.chart-mins'), function(value, key){
            var a = angular.element(value);
            timeLineArr.push(a);      
        });

        element.css({
            position: 'relative',
            display: 'block',
            cursor: 'pointer',            
            top: medLengthService.timeSliderVal - markerYOffset + 'px',
            left: '-'+ markerXOffset + 'px'
        });

        setTimeSnap () ;

        $ionicGesture.on('touch', function(e){           
            e.preventDefault();
            startX = e.gesture.deltaX - x;
            startY = e.gesture.deltaY - medLengthService.timeSliderVal;            
        }, element);

        $ionicGesture.on('drag', function(e) {           
            e.preventDefault();
            x = e.gesture.deltaX - startX;
            y = e.gesture.deltaY - startY;
            if( y < parentTop) { y = parentTop; }
            if( y > parentBot) { y = parentBot; }

            setTimeSnap();
           
            element.css({
                top: (medLengthService.timeSliderVal - markerYOffset )+ 'px',
                left: '-'+ markerXOffset + 'px'
            });

            medLengthService.timeSliderVal = y;
            scope.updateypos();           
        }, element);

        $ionicGesture.on('release', function(e) {
            e.gesture.deltaX = x;
            e.gesture.deltaY = y;
            setTimeSnap();                       
        }, element);


        function setTimeSnap () {

            for(var i =0; i < parentPercArr.length; i++)
            {               
                if(medLengthService.timeSliderVal < 0 ) {medLengthService.timeSliderVal = 0;}
                if(medLengthService.timeSliderVal > parentBot) {medLengthService.timeSliderVal = parentBot; }
               
                if(medLengthService.timeSliderVal < ((parentPercArr[i]) + parent5perc) && medLengthService.timeSliderVal >  ((parentPercArr[i]) - parent5perc) )
                {
                    medLengthService.timeSliderVal = parentPercArr[i];
                    medLengthService.timeSliderNum = percentValArr[i];                    
                    timeLineArr[i].addClass('mins-selected');                    
                } 
                else {
                    timeLineArr[i].removeClass('mins-selected');
                }
            }
        }
    };

}])


.directive('dayselector', ['$document', '$ionicGesture', 'dayQualityService',
    function ($document, $ionicGesture, dayQualityService) {

        return function (scope, element, attr) {

            var currY = attr.currypos;
            var startX = 0, startY = 0, x = 0, y = 0, markerYOffset, markerXOffset, parentDiv, parentTop, parentBot, parent1perc, parent50perc, parent90perc, parentPercArr, timeLineArr, timelineCurr;
            markerYOffset = element[0].offsetHeight / 2;
            markerXOffset = element[0].offsetWidth / 2;
            parentDiv = element.parent();
            parentTop = 50;
            parentBot = (parentDiv[0].offsetHeight) - markerYOffset*-0.8 ;
            parent1perc = parentBot /100;
            parent5perc = parent1perc * 5;
            parent15perc = parent1perc * 15;
            parent50perc = parent1perc * 50;
            parent90perc = parent1perc * 90;
            parentPercArr = [
                parent1perc,parent50perc,parent90perc
            ];
            percentValArr = [
                100,          50,          0
            ];
            timeLineArr = [];

            angular.forEach(document.querySelectorAll('.chart-mins'), function(value, key){
                var a = angular.element(value);
                timeLineArr.push(a);      
            });

            element.css({
                position: 'relative',
                display: 'block',
                cursor: 'pointer',            
                top: dayQualityService.timeSliderVal - markerYOffset + 'px',
                left: '-'+ markerXOffset + 'px'
            });

            setTimeSnap () ;


            $ionicGesture.on('touch', function(e){                
                e.preventDefault();
                startX = e.gesture.deltaX - x;
                startY = e.gesture.deltaY - dayQualityService.timeSliderVal;              
            }, element);

            $ionicGesture.on('drag', function(e) {             
                e.preventDefault();
                x = e.gesture.deltaX - startX;
                y = e.gesture.deltaY - startY;
                if( y < parentTop) { y = parentTop; }
                if( y > parentBot) { y = parentBot; }

                setTimeSnap();               
                element.css({
                    top: (dayQualityService.timeSliderVal - markerYOffset )+ 'px',
                    left: '-'+ markerXOffset + 'px'
                });

                dayQualityService.timeSliderVal = y;
                scope.updateypos();               
            }, element);

            $ionicGesture.on('release', function(e) {
                e.gesture.deltaX = x;
                e.gesture.deltaY = y;
                setTimeSnap();                
            }, element);


            function setTimeSnap () {

                for(var i =0; i < parentPercArr.length; i++)
                {                   
                    if(dayQualityService.timeSliderVal < 0 ) {dayQualityService.timeSliderVal = 0;}
                    if(dayQualityService.timeSliderVal > parentBot) {dayQualityService.timeSliderVal = parentBot; }
                   
                    if(dayQualityService.timeSliderVal < ((parentPercArr[i]) + parent15perc) && dayQualityService.timeSliderVal >  ((parentPercArr[i]) - parent15perc) )
                    {
                        dayQualityService.timeSliderVal = parentPercArr[i];
                        dayQualityService.timeSliderNum = percentValArr[i];                        
                        timeLineArr[i].addClass('mins-selected');                    
                    } 
                    else {
                        timeLineArr[i].removeClass('mins-selected');
                    }
                }
            }
        
        };
}])


.directive('svgstats', ['$document', 'ServicesData', '$timeout',
    function ($document, ServicesData, $timeout) {
   
    return function(scope, element, attrs) {

    //
    //  get jsondata from saved data  - keeps the Date field
    //     
    
    var jsonData = scope.stats;
    var jsonNoDate = scope.stats;

    var groupCount = 0;
    var itemCount = 0;    
    var itemGroupCount = 0;
    var groupHeight = 0;
    var minDataPoint = -10;
    var maxDataPoint = 35;  
    var paddingTop = 2;

    //
    //   convert data obj to arr of objects - without the Date
    //
    var jsonArr = [];
    var jsonMultArr = [];
    var jsonMultArrDate = [];

    for(var date in jsonData)
    {  
        if(date !== null || date !== undefined || date !== "null" || date !== "undefined" || date !== ""  )      
        {           
            var format = d3.time.format("%Y-%m-%d-%H-%M");  
            var arr2str = String(jsonData[date].medDate);            
            var str = String(format.parse(arr2str));           
            var charInd = str.lastIndexOf(":");            
            var cutStr = str.substring(0,charInd);          
            jsonNoDate[date].medDate = cutStr; //remove end chars           
        }
    }

    for(var data in jsonNoDate)
    { 
        jsonMultArrDate.push(d3.values(jsonNoDate[data]));       
        jsonNoDate[data].medDate = -10;   //set date to 0 //testing         
        jsonArr.push(jsonNoDate[data]);
        jsonMultArr.push(d3.values(jsonNoDate[data]));
    }    

    scanArr = function() {
        for(var i in jsonArr)
        {              
            var tempArr = jsonArr[i];         
            groupCount ++;
            for(var o in tempArr)
            {                
                itemCount ++;
            }
        }                       
    };   
    scanArr(); 


    var barH = 40;
    var contW = element[0].clientWidth;
    var contH = itemCount * barH;    
    
    var margin = {top: 40, right: 40, bottom: 10, left: 20},
        width = contW - margin.left - margin.right,
        height = (contH - margin.top - margin.bottom)+ barH;

    itemGroupCount = itemCount / groupCount;   
    groupHeight =  itemGroupCount * barH;


    var x0 = Math.max(Math.abs(minDataPoint), Math.abs(maxDataPoint));

    var x = d3.scale.linear()
    .domain([minDataPoint,x0])
    .range([0,width])
    .nice();

    var y = d3.scale.ordinal()
    .domain(d3.range(itemGroupCount))
    .rangeRoundBands([0, groupHeight], 0.2);

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("top");    

    $timeout(function () {

    // create SVG container 
        var svg = d3.select("#VisCont").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + (margin.bottom + 50))
        .append("g")
        .attr("transform", "translate("+ margin.left + "," + margin.top + ")");
        
        var gCont = svg.selectAll("g")
        .data(jsonMultArr)    
        .enter()
        .append("g")
            .attr("transform", function(d, i) {                         
                return "translate("+ 0 +","+ (groupHeight * i )  +")";
            })
            .attr("width", width + margin.left + margin.right )
            .attr("height", barH * itemGroupCount )
            .attr("x", 0)
            .attr("y", 0)        
        .selectAll("g")
        .data(function(d,i) {return d;})
        .enter()
        .append("rect")
            .attr('class', function(d,i) {                                    
                    if(d <= 0) { return 'statbar Neg';}
                    else if (d > 0 && d < 5) {
                        if(i == 1){return 'statbar med Pos';}
                        else if(i == 2) {return 'statbar len Pos';}
                        else if(i == 3) {return 'statbar tho Pos';}                        
                    }  
                    else if(i == 4 && d > 20) {return 'statbar qua good';}
                    else if(i == 4 && d > 10 && d < 20) {return 'statbar qua avg';}
                    else if(i == 4 && d < 10) {return 'statbar qua poor';}                   
                    else if (d == -10) {return 'statbar trans';}
                    else { return 'statbar Prog';}       
            })
            .attr('width', function(d) {                   
                    if(d <= 0 && d > -10) { return Math.abs(x(5) - x(0));}
                    else if(d == -10) {return Math.abs(x(0) - x(0));}
                    else if (d > 0 && d < 5 ) { return Math.abs(x(0) - x(5));} 
                    else {return Math.abs(x(d) - x(0));}                    
                })  
            .attr('height', y.rangeBand())
            .attr('x', function(d) {                 
                    if(d <= 0) { return x(Math.min(0, -5)); }
                    else if (d > 0 && d < 5) {return x(Math.min(0, d)); } 
                    else {return x(Math.min(0, d)) ; } 
                })
            .attr("y", function(d,i) {return barH * i;})
            .html(function(d,i) {return d;})
            .style({'opacity': 0,
                    'border':'1px solid #666666'
            })
            .transition()
                    .delay( function(d,i) {return i * 150;} )
                    .duration(1000)                  
                    .style('opacity', 1);
                    

            
        
        var txtCont = svg.selectAll("g")
        .data(jsonMultArrDate)    
        .selectAll("g")
        .data(function(d,i) {return d;})
        .enter()
        .append("text")
                .attr("class", function(d,i){
                    if(i === 0) {return "txtData lgtxt";}
                    else {return "txtData";}

                })
                .html(function(d, i) {
                    //if -num then NO, if +num then YES else MINS
                    var txt = setBarData(d, i);
                    return  txt;       
                })
                .attr('x', function(d) {                              
                    if(d.length >= 10) { return x(Math.min(1, 1)); }
                    else if (d <= 0) { return x(Math.min(0, -1)); }
                    else if (d > 0 && d < 5) { return x(Math.min(1, d)); }
                    else {return x(Math.max(1, d+0.5));}               
                })            
                .attr('y', function(d,i) {
                    var txtYoffset = 0;
                    if(i === 0) {txtYoffset = 12;}
                    return  (groupHeight / itemGroupCount) * i + ((barH/2)-2) + txtYoffset;               
                })
                .attr('text-anchor', function(d){
                    if(d <= 0) {return 'end';}
                    else if (d > 1 && d < 5) {return 'end';} ////////fix this
                    else { return 'start';}
                })
                .style('opacity', 0)
                .transition()
                    .delay( function(d,i) {return i * 150;} )
                    .duration(1000)
                    .style('opacity', 1);
        
        //
        //     create the divider lines
        //
        svg.selectAll("g").append("line")
        .data(jsonMultArr)
            .attr('x1', x(-10))
            .attr('y1', groupHeight + 12)
            .attr('x2', x(35))
            .attr('y2', groupHeight + 12 )
            .attr("stroke-width", 4)
            .attr("stroke", '#ffffff')
            .style('opacity', 0)
                .transition()
                    .delay( function(d,i) {return i * 150;} )
                    .duration(1000)
                    .style('opacity', 1);
            
            
        svg.selectAll("g").append("line")
        .data(jsonMultArr)
            .attr('x1', x(0))
            .attr('y1', barH)
            .attr('x2', x(35))
            .attr('y2', barH)
            .attr("stroke-width", 1)
            .attr("stroke", '#ffffff')
            .style('opacity', 0)
                .transition()
                    .delay( function(d,i) {return i * 150;} )
                    .duration(1000)
                    .style('opacity', 1);   
        

        svg.append("g")
        .attr("class", "y axis")
        .append("line")
            .attr("x1", x(0))
            .attr("x2", x(0))
            .attr("y1", 0)
            .attr("y2", height + 50)
            .attr("stroke-width", 2)
            .style('opacity', 0)
                .transition()
                    .delay( function(d,i) {return i * 150;} )
                    .duration(1000)
                    .style('opacity', 1);


        svg.append("g")
        .attr("class", "x axis")
        .call(xAxis)
        .append("text")
            .data(jsonMultArr)
            .attr('x', function(d) {return x(Math.max(0, 30))/2;})
            .attr('y', -25)
            .attr('text-anchor', 'middle')
            .attr('class', 'txtLabel')
            .text('meditation (mins)')
            .style('opacity', 0)
                .transition()
                    .delay( function(d,i) {return i * 150;} )
                    .duration(1000)
                    .style('opacity', 1);

            //
            //   use this to change the text output from numbers to strings
            //
            function setBarData(d, i) { 
                if(i != 4) {
                    switch(d) { 
                        case 0:
                            return 'NO';
                            
                        case -1:
                            return 'NO';
                            

                        case 1:
                            return 'YES';
                            

                        case -10:
                            return 'Date here';
                            

                        default:
                         //   return d + ' mins';
                    }
                    if(d.length >= 10) {
                        return d;
                    }  else {
                        return d + ' mins';
                    }
                }
                if(i == 4) {
                    if(d > 20){
                        return 'Good';
                    }else if (d > 10 && d < 20) {
                        return 'Average';
                    }else if (d < 10) {
                        return 'Poor';
                    }
                }                
            }
    },500);
    };
}]);






