angular.module('mylo.servicesConnect', [])

.factory('ServicesConnect', ['ServicesAuth', 'ServicesData', '$firebase', '$localstorage', '$window', '$document', '$ionicPopup', '$timeout', '$ionicBackdrop',
	function (ServicesAuth, ServicesData, $firebase, $localstorage, $window, $document, $ionicPopup, $timeout, $ionicBackdrop) {
	
	var servicesConnectInst = {};
	servicesConnectInst.onlineStatus = "offline";

	servicesConnectInst.check = function () {

		function updateOnlineStatus (e) {
			servicesConnectInst.onlineStatus = $window.navigator.onLine ? "online" : "offline";			

			 var appPopup = $ionicPopup.show({
	            title: 'The app is now '+ servicesConnectInst.onlineStatus
	        });
	        $timeout(function (){
	            appPopup.close(); 
	            var result = document.getElementsByClassName("backdrop");
	            var wrappedQueryResult = angular.element(result);	          
	            if(wrappedQueryResult.hasClass('visible')) {	            
	            	wrappedQueryResult.removeClass('visible');
	            }
	            if(wrappedQueryResult.hasClass('active')) {	            
	            	wrappedQueryResult.removeClass('active');
	            }
	        }, 3000);				
		}

		updateOnlineStatus();  //initial check
		$window.addEventListener('online', updateOnlineStatus);
		$window.addEventListener('offline', updateOnlineStatus);


		if ($window.cordova) {
			
			document.addEventListener("deviceReady", onDeviceReady, false);

			// device API ready
			onDeviceReady();			
			checkDeviceConnection();

		} else {
			
			if ($window.navigator.onLine) { //true				
				Firebase.goOnline(); // All Firebase instances automatically reconnect
				if(ServicesAuth.getLoggedInStatus() ){					
					ServicesData.setupData(true);
				}else{				
				}

				
			} else {
				 console.log("using localStorage :( ");
				Firebase.goOffline(); // All Firebase instances are disconnected
				if(ServicesAuth.getLoggedInStatus() ){
					ServicesData.setupData(false);
				}				
			}
		}		
	};


	servicesConnectInst.getConnect = function () {
		if(servicesConnectInst.onlineStatus === 'online'){
			return true;
		}		
		else {
			return false;
		}
	};

	function onDeviceReady () {
		checkDeviceConnection();

		//set initial data source
		if (navigator.onLine) {
			
			Firebase.goOnline();
			if(ServicesAuth.getLoggedInStatus() ){					
				ServicesData.setupData(true);
			}else{
				console.log('Mobile using firebase, but not logged in yet');
			}
		} else {					
			Firebase.goOffline();
			ServicesData.setupData(false);					
		}		
	}
	
	function checkDeviceConnection () {
		var networkState = navigator.connection.type;

		var states = {};
        states[Connection.UNKNOWN]  = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI]     = 'WiFi connection';
        states[Connection.CELL_2G]  = 'Cell 2G connection';
        states[Connection.CELL_3G]  = 'Cell 3G connection';
        states[Connection.CELL_4G]  = 'Cell 4G connection';
        states[Connection.CELL]     = 'Cell generic connection';
        states[Connection.NONE]     = 'No network connection';
	}
	
	

	return servicesConnectInst;
}]);

