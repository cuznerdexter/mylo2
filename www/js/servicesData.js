angular.module('mylo.servicesData', [])
.factory('ServicesData', ['$localstorage', '$firebase', '$firebaseAuth', 'ServicesAuth', '$window', '$http', 
    function ($localstorage, $firebase, $firebaseAuth, ServicesAuth, $window, $http) {
	
	var servicesData = {};
    servicesData.mylodata = {};

    servicesData.mylokey = [
        {
            "pageName"  : "meditation started",
            "boxCol"    : "med"
        },
        {
            "pageName"  : "meditation length",
            "boxCol"    : "len"
        },
        {
            "pageName"  : "thoughtless moments",
            "boxCol"    : "tho"
        },
        {
            "pageName"  : "",
            "boxCol"    : "qua poor"
        },
        {
            "pageName"  : "",
            "boxCol"    : "qua avg"
        },
        {
            "pageName"  : "day quality",
            "boxCol"    : "qua good"
        }
    ];

    servicesData.simpleObj = function () {
        return {value: 'SIMPLE!'};
    };
    
    
    servicesData.checkAccess = function () {
        if($window.navigator.onLine && ServicesAuth.getLoggedInStatus() )
            { return true;}
        else if (!$window.navigator.onLine && ServicesAuth.getLoggedInStatus())
            {return false;}        
    };
    

    servicesData.setupData = function (isLoggedIn) {
        if(isLoggedIn) {
             console.log('get data from Firebase');
            
            ServicesAuth.userPath.on('value', 
                function (snapshot){                  
                    $localstorage.setObject( "mylodata", snapshot.val() );                    
            }, function (errorObject){
                    console.log("The read failed: " + errorObject.code);
            });
            
        } else {            
            servicesData.mylodata = $localstorage.getObject("mylodata");          
        }
    };

    servicesData.getDate = function () {
        var meditateDate = new Date();        
        var dateStr = meditateDate.getFullYear()+'-'+(meditateDate.getMonth() +1)+'-'+meditateDate.getDate()+'-'+meditateDate.getHours()+'-'+meditateDate.getMinutes();
        ServicesAuth.statsCurrDate = dateStr;
        return dateStr;
    };

    servicesData.getSnapshot = function (path) {
        
        path.on('value', 
            function (snapshot) {
              
                var mylodata = snapshot.val();
                $localstorage.setObject("mylodata", mylodata);             
                return mylodata;//snapshot.val();
            });
    };
    servicesData.updateSnapshot = function (path, updatedObj) {
    
        path.update(updatedObj);
        ServicesAuth.userPath.on('value', 
                function (snapshot){                       
                    $localstorage.setObject( "mylodata", snapshot.val() );                 
            }, function (errorObject){              
            });
    };

    servicesData.getLocal = function () {     
        $localstorage.getObject( "mylodata" );
    };

    servicesData.setStatsInit = function () {
        if( servicesData.checkAccess() ) {           
            servicesData.getDate();
            ServicesAuth.statsPath = new Firebase(ServicesAuth.baseUrl + '/users/' + ServicesAuth.CurrUserId + '/stats/' + ServicesAuth.statsCurrDate);
            servicesData.updateSnapshot(ServicesAuth.statsPath, { "medDate": servicesData.getDate() });            
          
        }  else {
          
        }
    };


    servicesData.setStatsMed = function (bool) {
        if( servicesData.checkAccess() ) {
            ServicesAuth.statsEntryPath = new Firebase(ServicesAuth.baseUrl + '/users/' + ServicesAuth.CurrUserId + '/stats/' + ServicesAuth.statsCurrDate);
            if(bool === true) {                    
                servicesData.updateSnapshot(ServicesAuth.statsEntryPath, {"medInit" : bool});           
            }else{
                servicesData.updateSnapshot(ServicesAuth.statsEntryPath, {
                    "medInit" : bool,
                    "medLen"  : 0,
                    "medTho"  : false,
                    "medUql"  : 0
                });
            }            

        }else {
        
        }
    }; 


    servicesData.setStatsMedLen = function (time) {
        if( servicesData.checkAccess() ) {
            ServicesAuth.statsEntryPath = new Firebase(ServicesAuth.baseUrl + '/users/' + ServicesAuth.CurrUserId + '/stats/' + ServicesAuth.statsCurrDate);
            if(time !== 0){            
                servicesData.updateSnapshot(ServicesAuth.statsEntryPath, {"medLen": time});              
            }else{
                servicesData.updateSnapshot(ServicesAuth.statsEntryPath, {
                    "medLen": time,
                    "medTho"  : false,
                    "medUql"  : 0
                });   
            }
        }else {

        }
        
            
    };


    servicesData.setStatsMedTho = function (bool) {
        if( servicesData.checkAccess() ) {
            ServicesAuth.statsEntryPath = new Firebase(ServicesAuth.baseUrl + '/users/' + ServicesAuth.CurrUserId + '/stats/' + ServicesAuth.statsCurrDate);
            if(bool === true) {                       
                servicesData.updateSnapshot(ServicesAuth.statsEntryPath, {"medTho" : bool});              
            }else{
                servicesData.updateSnapshot(ServicesAuth.statsEntryPath, {
                    "medTho" : bool,
                    "medUql"  : 0
                });
            }
        }else {
        }
    }; 


    servicesData.setStatsMedQual = function (quality) {
         if( servicesData.checkAccess() ) {
            console.log('online & logged in: firebase:  create medQua');
            ServicesAuth.statsEntryPath = new Firebase(ServicesAuth.baseUrl + '/users/' + ServicesAuth.CurrUserId + '/stats/' + ServicesAuth.statsCurrDate);
            servicesData.updateSnapshot(ServicesAuth.statsEntryPath, {"medUql": quality});   
         
         }else{

         } 
    };

    servicesData.getStatsAll = function () {
 
        servicesData.getLocal();
    };


    servicesData.getJSON = function () {
        var statJSON = $localstorage.getObject("mylodata");        
        var statsColl = statJSON.stats;

        for(var dateObj in statsColl)
        {           
            var statObj = statsColl[dateObj];
            if(statObj.hasOwnProperty("medInit"))
            {             
                if(statObj.medInit === true)
                    {statObj.medInit = 1;}
                else {
                    statObj.medInit = 0;
                }
            }
            if(statObj.hasOwnProperty("medTho"))
            {            
                if(statObj.medTho === true)
                    {statObj.medTho = 1;}
                else {
                    statObj.medTho = 0;
                }
            }            
        }        
        return statJSON.stats;
    };    
	return servicesData;
}])


.factory('$localstorage', ['$window', 
function($window) {
    return {
        set: function (key, value) {
            $window.localStorage[key] = value;
        },
        get: function (key, defaultValue) {
            return $window.localStorage[key] || defaultValue;
        },
        setObject: function (key, value) {
            $window.localStorage[key] = JSON.stringify(value);
        },
        getObject: function (key) {
            return JSON.parse($window.localStorage[key] || '{}');
        }
    };
}])



.factory('medLengthService', [ function () {
     this.medLengthData = {};
    this.medLengthData.timeSliderVal = 0;
    this.medLengthData.timeSliderNum = 0;   
    return this.medLengthData;
}])



.factory('dayQualityService', [ function () {
 
    this.dayQualityData = {};
    this.dayQualityData.timeSliderVal = 0;
    this.dayQualityData.timeSliderNum = 0; 
    this.dayQualityData.dayValue = '';   
    return this.dayQualityData;
}])




.factory('reflectionService', [ function () {
 
    this.reflectionData = {};   

    this.reflectionData.dataType = 'Test';
    this.reflectionData.dataSet = [
        [
            'Maybe try again tomorrow'
        ],
        [
            'Try and make it last longer'
        ],
        [
            'The more you meditate the easier it will get'
        ]
    ];

    return this.reflectionData;
}])


.factory('adviceService', [
    function () {
    this.adviceData = {};

    this.adviceData.dataSet = [
        ["Did you know that it helps if you use an ice pack to slow down your thoughts if they're too busy? "],          
        ["Did you know that lots of light can really help your meditation if it feels too lethargic or you're feeling a little depressed? Try adding two more candles to your meditation environment. "],
        ["Do remember that it's MUCH better to meditate every day for a short time, rather than miss a day and try and catch up with a longer meditation. Little and often delivers incredibly powerful results, so try and make it regular. But if you miss one, don't worry, just keep going!"]
    ];

    return this.adviceData;
}])


.factory('notifyService', ['$http', 
    function ($http) {      
        var notify = {};
        notify.message = '';
        notify.getMessages = function () {
        $http.get('js/notifyMessages.json')
        .success(function (data) {
            console.log('success notify data loaded: ', data.notifyMessages );
            notify.message = data.notifyMessages[0];
            console.log(notify.message);
        })
        .error(function (error) {
            console.log('error: ',error);
        });
    };
    return notify;
}]);