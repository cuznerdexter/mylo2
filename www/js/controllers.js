angular.module('mylo.controllers', [])

.controller('MainController', ['$scope', '$state', '$rootScope', '$ionicPlatform', '$ionicPopup', '$timeout', 'ServicesAuth', 'ServicesConnect', 'ServicesData', 'notifyService', '$cordovaLocalNotification', '$ionicSideMenuDelegate', '$firebase',
    function ($scope, $state, $rootScope, $ionicPlatform, $ionicPopup, $timeout, ServicesAuth, ServicesConnect, ServicesData, notifyService, $cordovaLocalNotification, $ionicSideMenuDelegate, $firebase) {
    

        $scope.parentObj = {};  
        $scope.parentObj.auth = ServicesAuth.Auth;  
        $scope.parentObj.user = $scope.parentObj.auth.$getAuth();
        $scope.parentObj.loginState = $scope.parentObj.auth.$getAuth();
        $scope.parentObj.connectState = ServicesConnect.getConnect();
        $scope.parentObj.simplelogin = '';
        $scope.parentObj.online = true;   

        $scope.parentObj.alarmTime = '';       

        $scope.parentObj.displayName = ServicesAuth.CurrName;  
        $scope.parentObj.yepNopePage = '';
        $scope.parentObj.homeState = '';
        $scope.parentObj.colorVal1 = 'redBg';
        $scope.parentObj.colorVal2 = 'greenBg';
        $scope.parentObj.colorVal3 = 'blueBg';

        $scope.parentObj.reflection = '';
        $scope.parentObj.advice = '';
       
        $scope.notifyMessages = notifyService.getMessages();        

        $scope.parentObj.init = function () {
            $scope.parentObj.dragbool = $state.current.views.mainContent.data.dragbool;
            $scope.parentObj.yepNopePage = $state.current.views.mainContent.data.yepNopePage;
            $scope.parentObj.colorVal1 = $state.current.views.mainContent.data.colorVal[0];
            $scope.parentObj.colorVal2 = $state.current.views.mainContent.data.colorVal[1];
            $scope.parentObj.colorVal3 = $state.current.views.mainContent.data.colorVal[2];
            $scope.parentObj.homeState = $state.current.views.mainContent.data.homeState;
        };

        $ionicPlatform.ready(function() {
            if(navigator.splashscreen) {
              navigator.splashscreen.hide();
            }

            $scope.$on("$cordovaLocalNotification:schedule", function (event, notification, state) {             
            });
            $scope.$on("NOTIFYTIME", function (alarmData) {
                $scope.parentObj.alarmTime = alarmData.targetScope.notifyTime;
               
                var str = $scope.parentObj.alarmTime.toString();
                str = str.replace(/[/]/g, ',');                  
                var d = str.toLocaleString('en-US');                
                $scope.parentObj.alarmTime = d;               
                $scope.addNote($scope.parentObj.alarmTime);
            });

            $scope.addNote = function (alarm) {                
                $cordovaLocalNotification.schedule({
                    id: 1,
                    title: 'How was your meditation today?',
                    text:   notifyService.message,
                    every: 'day',
                    led: 'FD0174'
                }).then(function () {
                    console.log("The notification has been set");
                });
            };

            $scope.isScheduled = function () {
                $cordovaLocalNotification.isScheduled(1).then(function (isScheduled) {                   
                });
            };

            document.addEventListener("menubutton", onMenuKeyDown, false);
            document.addEventListener("backbutton", onBackKeyDown, false);
            document.addEventListener("pause", pauseApp, false);
            document.addEventListener("resume", resumeApp, false);

            

        });

        function onMenuKeyDown () {
            //activated when menu button pressed
            console.log('menu button pressed - toggle menu');
            $ionicSideMenuDelegate.toggleLeft();
        };

        function onBackKeyDown () {
            //activated when back button pressed
            console.log('back button pressed');
        };

        function pauseApp () {
            console.log('MYLO has been paused');
            Firebase.goOffline();
        };

        function resumeApp () {
            console.log('MYLO has resumed');
            Firebase.goOnline();
        };


        


}])

.controller('LoginCtrl', ['$scope', '$state', '$q', '$timeout', 'ServicesAuth', 'ServicesConnect', 
    function ($scope, $state, $q, $timeout, ServicesAuth, ServicesConnect) {        
      
       $scope.parentObj.init();
       $scope.parentObj.connectState = ServicesConnect.getConnect();
       
       $scope.form = {};
            $scope.form.email = null;
            $scope.form.password = null;
        
        $scope.loginGrp = ServicesAuth.loginGrp;         

        $scope.goHome = function(error, authData) {
            if(authData)
            {             
            }else {            
            }
        };

        $scope.createUser = function () {
            $scope.message = null;
            
            $scope.parentObj.auth.$createUser({
                email: $scope.form.email,
                password: $scope.form.password
              }).then(function(userData) {                 
                $scope.message = "User created with uid: " + userData.uid;               
              }).catch(function(error) {               
                $scope.error = error;
              });
        };

        $scope.removeUser = function () {
            $scope.message = null;
            $scope.error = null;

            $scope.parentObj.auth.$removeUser({
                email: $scope.form.email,
                password: $scope.form.password
            }).then(function() {
                $scope.message = "User removed";
              }).catch(function(error) {
                $scope.error = error;
              });
        };

        $scope.loginWithPass = function () {            
            $scope.message = null;
            $scope.error = null;
        
            $scope.parentObj.auth.$authWithPassword({
                email: $scope.form.email,
                password: $scope.form.password
            }, function(error, authData) {            
              if (error) {
                console.log("Login Failed!", error);
              } else {
                console.log("Authenticated successfully with payload:", authData);              
              }                
            });           
        };
       
        $scope.logOut = function() {
            ServicesAuth.Logout();
        };        

        $scope.Anonamous = function() {         
            ServicesAuth.Anonamous();          
            ServicesAuth.UpdateData();          
            $state.go('app.home');
        };

    $scope.parentObj.loginState = $scope.parentObj.auth.$getAuth();
    $scope.parentObj.user = $scope.parentObj.auth.$getAuth();   
}])

.controller('IntroCtrl', ['$scope','$state', '$ionicSlideBoxDelegate',
    function ($scope, $state, $ionicSlideBoxDelegate) {       
        $scope.parentObj.firstRun = !$scope.parentObj.firstRun;
        
        $scope.startApp = function() {
            $state.go('app.home');
        };
        $scope.next = function() {
            $ionicSlideBoxDelegate.next();            
        };
        $scope.previous = function() {
            $ionicSlideBoxDelegate.previous();           
        };
        $scope.slideChanged = function(index) {
            $scope.slideIndex = index;            
        };
}])

.controller('AppCtrl', ['$scope', '$cordovaDatePicker', '$ionicPlatform', 'ServicesAuth', 'notifyService', 
    function ($scope, $cordovaDatePicker, $ionicPlatform, ServicesAuth, notifyService ) {
       
        $scope.notifyState = false;
        $scope.reminderState = false;
        $scope.mobileMode = window.cordova ? true : false;
        $scope.notify = "OFF";
        $scope.notifyTime = "Set the time";
        $scope.parentObj.loginState = $scope.parentObj.auth.$getAuth();
        $scope.logState = ['Login',false];

        $scope.$watch( function (scope) {return $scope.parentObj.loginState;},
            function (newValue, oldValue) {
                var result = document.getElementsByClassName("myloPrivate");
                var wrappedQueryResult = angular.element(result);               
                if(newValue !== null) {                
                    $scope.logState = ['Logout',true];
                    wrappedQueryResult.removeClass('myloSideDisabled');
                }else {
                    $scope.logState = ['Login',false];                              
                    wrappedQueryResult.addClass('myloSideDisabled');
                }
            }
        );       

        $scope.setReminderPanel = function () {
            $scope.reminderState = !$scope.reminderState;            
        };

        $scope.setNotify = function () {
            $scope.notifyState = !$scope.notifyState;
            if($scope.notifyState)
            {
                $scope.notify = "ON";
            }else
            {
                $scope.notify = "OFF";
            }
        };

        $scope.externalLink = function (url) {
            if($ionicPlatform)
            {
                window.open(url, "_system");
            }else {
                console.log('needs ngCordova inAppBrowser');
            }            
        };

        $ionicPlatform.ready(function() {
            $scope.setNotificationTime = function () {
                var options = {
                    date: new Date(),
                    mode: 'time', // or 'time'
                    minDate: new Date() - 10000,
                    allowOldDates: true,
                    allowFutureDates: false,
                    doneButtonLabel: 'DONE',
                    doneButtonColor: '#F2F3F4',
                    cancelButtonLabel: 'CANCEL',
                    cancelButtonColor: '#000000'
                };
               
                $cordovaDatePicker.show(options).then(function(date){
                    alert(date);                   
                    $scope.notifyTime = options.date;                   
                    $scope.$emit('NOTIFYTIME', $scope.notifyTime);
                });
            };
        });
}])

.controller('HomeCtrl', ['$scope', '$state', 'ServicesData',
    function ($scope, $state, ServicesData) {       
        $scope.parentObj.init();
        $scope.linkNewEntry = $state.current.views.mainContent.data.myloLinks.newEntry;
        $scope.linkViewStats = $state.current.views.mainContent.data.myloLinks.viewStats;

        $scope.parentObj.loginState = $scope.parentObj.auth.$getAuth();
        $scope.parentObj.user = $scope.parentObj.auth.$getAuth();

        $scope.createEntry = function () {      
            ServicesData.setStatsInit();
        };
}])


.controller('MeditatedCtrl', ['$scope', '$state', 'reflectionService', 'ServicesData',
    function ($scope, $state, reflectionService, ServicesData) {
        $scope.parentObj.init();
        $scope.medEventYes = $state.current.views.mainContent.data.myloLinks.medEventYes;
        $scope.medEventNo = $state.current.views.mainContent.data.myloLinks.medEventNo;
        
        $scope.setAnswer = function (answer) {
            $scope.answered(answer);
        };
        $scope.onSwipeAcross = function (answer) {            
            $scope.answered(answer);
        };
        $scope.answered = function (answer) {
            if(answer == 'yes'){
                ServicesData.setStatsMed(true);
                $state.go('app.meditationLength');
            }else {
                ServicesData.setStatsMed(false);
                $state.go('app.home');
            }
        }; 
        reflectionService.dataType = 0;
}])

.controller('MeditationLengthCtrl', ['$scope', '$state', 'reflectionService', 'medLengthService', 'ServicesData', 
    function ($scope, $state, reflectionService, medLengthService, ServicesData) {
        $scope.parentObj.init();
        $scope.currYposition = 0;       
        if(medLengthService.timeSliderNum == 'undefined')
        {medLengthService.timeSliderNum = 0;}
        $scope.ansNum = medLengthService.timeSliderNum;

        $scope.updateypos = function () {
            $scope.currYposition = medLengthService.timeSliderVal; 
        };        
        $scope.setAnswer = function () {
            $scope.answered();
        };
        $scope.onSwipeAcross = function () {            
            $scope.answered();
        };
        $scope.answered = function () {
            $scope.updateypos();          
            if(medLengthService.timeSliderNum < 5 ){
                $scope.ansNum = medLengthService.timeSliderNum;                
                ServicesData.setStatsMedLen($scope.ansNum);   
                $state.go('app.meditationReflect');                
            }else {
                $scope.ansNum = medLengthService.timeSliderNum;              
                ServicesData.setStatsMedLen($scope.ansNum);         
                $state.go('app.meditationThoughtless');
            }
        }; 
                
        reflectionService.dataType = 1;
}])



.controller('MeditationThoughtlessCtrl', ['$scope', '$state', 'reflectionService', 'ServicesData',
    function ($scope, $state, reflectionService, ServicesData) { 
        $scope.parentObj.init();
        $scope.medEventYes = $state.current.views.mainContent.data.myloLinks.medEventYes;
        $scope.medEventNo = $state.current.views.mainContent.data.myloLinks.medEventNo;

        $scope.setAnswer = function (answer) {
            $scope.answered(answer);
        };
        $scope.onSwipeAcross = function (answer) {            
            $scope.answered(answer);
        };
        $scope.answered = function (answer) {
            if(answer == 'yes'){
                ServicesData.setStatsMedTho(true);
                $state.go('app.meditationDay');
            }else {
                ServicesData.setStatsMedTho(false);
                $state.go('app.meditationReflect');
            }
        }; 
        reflectionService.dataType = 2;
}])


.controller('MeditationDayCtrl', ['$scope', '$state', 'reflectionService', 'dayQualityService', 'ServicesData', 
    function ($scope, $state, reflectionService, dayQualityService, ServicesData) {
        $scope.parentObj.init();

        $scope.updateypos = function () {
            $scope.currYposition = dayQualityService.timeSliderVal;          
        };

        $scope.setAnswer = function () {
            $scope.answered();
        };

        $scope.answered = function () { 
            switch (dayQualityService.timeSliderNum) {
                case 100:
                    dayQualityService.dayValue = 'good';
                    ServicesData.setStatsMedQual(35);
                    break;
                case 50:
                    dayQualityService.dayValue = 'average';
                    ServicesData.setStatsMedQual(17);
                    break;
                case 0:
                    dayQualityService.dayValue = 'poor';
                    ServicesData.setStatsMedQual(5);
                    break;
            }       
        };        
}])



.controller('MeditationStatsCtrl', ['$scope', '$state', 'ServicesData', 'loadData', '$timeout',
    function ($scope, $state, ServicesData, loadData, $timeout ) {
       
        $scope.parentObj.init();        
        $scope.medEventAdvice = $state.current.views.mainContent.data.myloLinks.medEventAdvice;
        
        $scope.stats =  ServicesData.getJSON();       
        $scope.myloKey = ServicesData.mylokey;       

        $scope.onSwipeAcross = function (answer) {            
            if(answer == 'yes'){
                $state.go('app.meditationAdvice');
            }else {
                $state.go('app.home');
            }
        };
}])


.controller('MeditationReflectCtrl', ['$scope', '$state', 'reflectionService',
    function ($scope, $state, reflectionService) {
        $scope.parentObj.init();

        $scope.setReflection = function () {                      
            $scope.parentObj.reflection = reflectionService.dataSet[reflectionService.dataType][0];
         };

         $scope.viewStats = function () {
            $state.go('app.meditationStats');
         };
        $scope.setReflection(); 
}])

.controller('MeditationAdviceCtrl', ['$scope', '$state', 'adviceService',
    function ($scope, $state, adviceService) {
        $scope.parentObj.init();
        $scope.medEventHome = $state.current.views.mainContent.data.myloLinks.medEventHome;

        $scope.adviceNum = 0;
        function mesgeMix (min, max) {            
            var rand = Math.floor(Math.random() * (max - min)) + min;            
            $scope.adviceNum = rand;
            return rand;
        }

        $scope.setAdvice = function () {                      
            $scope.parentObj.advice = adviceService.dataSet[mesgeMix(0, adviceService.dataSet.length )][0];
         };
        $scope.setAdvice();
}])

.controller('MeditationHowCtrl', ['$scope', '$state',
    function ($scope, $state) {
        $scope.parentObj.init();

        $scope.nxtPage = function () {
            $state.go('app.meditationGuide');
        };
}])

.controller('MeditationGuideCtrl', ['$scope', '$state',
    function ($scope, $state) {
        $scope.parentObj.init();

        $scope.nxtPage = function () {
            $state.go('app.home');
        };
}])

.controller('MeditationFeedbackCtrl', ['$scope', '$state', '$cordovaEmailComposer', '$ionicPlatform', '$location', '$anchorScroll',
    function ($scope, $state, $cordovaEmailComposer, $ionicPlatform, $location, $anchorScroll) {
        $scope.parentObj.init();

        $scope.feedback = {};

        $scope.pageShift = function (event, dir) {          
            $('ion-content').animate({                 
                scrollTop: dir
            }, 750);            
        };


        $scope.submitComments = function () {
            if(window.cordova) {
      
                $ionicPlatform.ready(function() {              
                    $cordovaEmailComposer.isAvailable().then(function() {                     
                       console.log($scope.feedback);
                       $cordovaEmailComposer.open({
                            to: 'cuznerdexter@gmail.com',
                            cc: [$scope.feedback.email, 'nigelp@gmail.com'],
                            bcc: [],
                            attachments: [],
                            subject: 'Mylo Feedback',
                            body: $scope.feedback.user +'<br>'+ $scope.feedback.name +'<br>'+ $scope.feedback.email +'<br>'+ $scope.feedback.comments,
                            isHtml: true
                       });
                     }, function () {
                       // not available
                       console.log('email isNOTAvailable ');
                     });
                    
                }, function(){
                    console.log('ionic is not ready, is this a mobile device?');
                });

            } else {
                console.log('no ionic');
            }
        };
}])


.controller('AboutCtrl', [ '$scope', 'aboutData', '$timeout',
    function ($scope, aboutData, $timeout) {
        console.log('AboutCtrl::');

        
            $scope.aboutData = aboutData.data.aboutPage.content;
            console.log($scope.aboutData);
        
        
}]);
