
describe('Directives: swapColour', function() {
	
	var compiled, rootScope, scope, elem, html, attr, attrInt, pageArr, arrObj, arrKey, arrVal; 

	beforeEach(module('mylo.directives'));
	
	html = '<div data-mylonxtpage="3" ></div>';
	pageArr = [{home:'redBg'}, {meditated:'greenBg'}, {meditationLength:'yellBg'}, {meditationThoughtless:'blueBg'}, {meditationStats:'purpBg'}, {login:'blueBg'}];

	beforeEach(inject(function($compile, $rootScope ) {		
		
		scope = $rootScope.$new();
		elem = angular.element(html);
		attr = elem.attr('data-mylonxtpage');		
		compiled = $compile(elem);
		compiled(scope);
		scope.$digest();

		attrInt = parseInt(attr);
		arrObj = pageArr[attrInt];
		arrKey = Object.keys(arrObj);
		arrVal = arrObj[arrKey];
		scope.colorVal = arrVal;
	}));

	it('checks attr mylonxtpage is of type number', function() {		
		expect(attrInt).toBe(3);
	});
	it('gets colour from array using mylonxtpage var', function() {
		expect(arrVal).toBe('blueBg');	
	});
	it('sets background colour of page using colorVal', function() {
		expect(scope.colorVal).toMatch(arrVal);
	});
});
